import imp
from employee import app
from flask import render_template,redirect,url_for,flash
from employee.models import Item
from employee.forms import RegisterForm
from employee import db

@app.route('/')
@app.route('/home')
def home_page():
    return render_template('home.html')

@app.route('/emploee')
def employee_page():
    user = Item.query.all()
    return render_template('employee.html', users=user)

@app.route('/register',methods=['GET','POST'])
def register_page():
    form = RegisterForm()
    if form.validate_on_submit():
        user_to_create=Item( 
                             firstname=form.firstname.data,
                             lastname=form.lastname.data,
                             email_address=form.email_address.data,
                             mobile=form.mobile.data,
                             address=form.address.data)
        db.session.add(user_to_create)
        db.session.commit()
        return redirect(url_for('employee_page'))
    if form.errors!={}:
        for err_msg in form.errors.values():
            flash(f'there was error with adding employee: {err_msg}')
    
    return render_template('register.html', form=form)