from employee import db

class Item(db.Model):
    
    firstname = db.Column(db.String(length=30), nullable=False)
    lastname = db.Column(db.String(length=30), nullable=False)

    email_address = db.Column(db.String(length=50), nullable=False, unique=True,primary_key=True)
    mobile = db.Column(db.String(length=60), nullable=False)
    address = db.Column(db.Integer(), nullable=False)
    def __repr__(self):
        return f'Item {self.name}'