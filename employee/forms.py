from cProfile import label
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import length,Email,DataRequired


class RegisterForm(FlaskForm):
    
    firstname = StringField(label='first Name:',validators=[DataRequired()])
    lastname = StringField(label='last Name:',validators=[DataRequired()])
    email_address = StringField(label='Email Address:',validators=[DataRequired()])
    mobile = StringField(label='Mobile Number:',validators=[DataRequired()])
    address = StringField(label='Address:',validators=[DataRequired()])
    submit = SubmitField(label='Submit')